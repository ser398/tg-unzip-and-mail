# Tg-unzip-and-mail

## Introduction
`Tg-uzip-and-mail` is a simple telegram bot that sends files to email, unzip archive if necessary.

## Installation
```bash
go get -u github.com/go-telegram-bot-api/telegram-bot-api
go get -u gopkg.in/gomail.v2
go get -u golang.org/x/net/proxy
go get -u golang.org/x/crypto/ssh/terminal
go get -u gitlab.com/ser398/tg-unzip-and-mail
```

## Configuration file
Example:
```json
{
    "telegram_token": "YOUR_TELEGRAM_TOKEN",
    "username": "YOUR_FULL_EMAIL",
    "target_mail": "TARGET_EMAIL",
    "STP_server": "STP_SERVER_OF_YOUR_EMAIL",
    "password": "PASSWORD_FOR_YOUR_EMAIL",
    "STP_port": PORT_FOR_STP_SERVER,
    "allow_list": [ALLOWED_ID1, ALLOWED_2],
    "proxy_address": "PROXY_ADRESS"
}

```
If `"allow_list"` is empty, then all users are allowed.
If `"proxy_adr"` is empty, then the proxy will not be used.
All other fields are required and will be requested if omitted.
