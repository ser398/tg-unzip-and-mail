package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	terminal "golang.org/x/crypto/ssh/terminal"
	proxy "golang.org/x/net/proxy"
	"gopkg.in/gomail.v2"
)

type httpClient interface {
	Do(req *http.Request) (*http.Response, error)
	Get(url string) (resp *http.Response, err error)
}

func downloadFile(filepath string, url string, client httpClient) error {

	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	out, err := os.Create(filepath)
	if err != nil {

		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	return err
}

type config struct {
	TelegramToken string `json:"telegram_token"`
	Username      string `json:"username"`
	TargetMail    string `json:"target_mail"`
	AllowList     []int  `json:"allow_list"`
	Password      string `json:"password"`
	STPServer     string `json:"STP_server"`
	STPPort       int    `json:"STP_port"`
	ProxyAdress   string `json:"proxy_address"`
}

func main() {
	var config config
	configurationFile, err := os.Open("config.json")
	if err != nil {
		log.Print(err)
	} else {
		bytes, err := ioutil.ReadAll(configurationFile)
		if err != nil {
			log.Fatal(err)
		}
		err = json.Unmarshal(bytes, &config)
		if err != nil {
			log.Fatal(err)
		}
	}

	reader := bufio.NewReader(os.Stdin)
	if config.Username == "" {
		fmt.Printf("Enter the full email adress: ")
		config.Username, err = reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		config.Username = strings.TrimSuffix(config.Username, "\n")
	}
	if config.Password == "" {
		fmt.Printf("Enter the password to %s: ", config.Username)
		bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			log.Fatal(err)
		}
		config.Password = string(bytePassword)
	}
	if config.STPServer == "" {
		fmt.Printf("Enter the STP server adress: ")
		config.STPServer, err = reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		config.STPServer = strings.TrimSuffix(config.STPServer, "\n")
		fmt.Printf("Enter the STP server port: ")
		port, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		port = strings.TrimSuffix(port, "\n")
		config.STPPort, err = strconv.Atoi(port)
		if err != nil {
			log.Fatal(err)
		}
	}
	if config.TargetMail == "" {
		fmt.Printf("Enter the full target email adress: ")
		config.TargetMail, err = reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		config.TargetMail = strings.TrimSuffix(config.TargetMail, "\n")
	}

	d := gomail.NewPlainDialer(config.STPServer, config.STPPort, config.Username, config.Password)
	_, err = d.Dial()
	if err != nil {
		log.Fatal(err)
	} else {
		log.Printf("Authorized on email account.")
	}
	client := &http.Client{}
	if config.ProxyAdress != "" {
		dialer, err := proxy.SOCKS5("tcp", config.ProxyAdress, nil, proxy.Direct)
		if err != nil {
			log.Fatal(err)
		}

		tr := &http.Transport{
			Dial: dialer.Dial,
		}
		client = &http.Client{Transport: tr}
	}
	if config.TelegramToken == "" {
		fmt.Printf("Enter the telegram token: ")
		config.TelegramToken, err = reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		config.TelegramToken = strings.TrimSuffix(config.TelegramToken, "\n")
	}

	bot, err := tgbotapi.NewBotAPIWithClient(config.TelegramToken, tgbotapi.APIEndpoint, client)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true
	log.Printf("Authorized on account %s", bot.Self.UserName)

	var ucfg tgbotapi.UpdateConfig = tgbotapi.NewUpdate(0)
	ucfg.Timeout = 60

	updates, err := bot.GetUpdatesChan(ucfg)

	for update := range updates {
		if update.Message == nil {
			continue
		}
		if len(config.AllowList) != 0 {
			allow := false
			for _, id := range config.AllowList {
				allow = allow || (id == update.Message.From.ID)
			}
			if !allow {
				continue
			}
		}

		fileID := update.Message.Document.FileID
		filename := update.Message.Document.FileName
		fullFilename := filepath.Join("/tmp", filename)
		if err != nil {
			log.Panic(err)
		}
		fileURL, err := bot.GetFileDirectURL(fileID)
		if err != nil {
			log.Panic(err)
		}
		err = downloadFile(fullFilename, fileURL, client)
		if err != nil {
			log.Fatal(err)
		}

		folderPath, err := ioutil.TempDir("/tmp", "unzipper")
		if err != nil {
			log.Fatal(err)
		}
		log.Print(update.Message.Document.MimeType)
		if update.Message.Document.MimeType == "application/zip" {
			cmd := exec.Command("unzip", fullFilename, "-d", folderPath)
			if err = cmd.Run(); err != nil {
				log.Fatal(err)
			}
		} else {
			cmd := exec.Command("mv", fullFilename, folderPath)
			if err = cmd.Run(); err != nil {
				log.Fatal(err)
			}
		}
		items, err := ioutil.ReadDir(folderPath)
		if err != nil {
			log.Fatal(err)
		}
		for _, item := range items {
			m := gomail.NewMessage()
			m.SetHeader("From", config.Username)
			m.SetHeader("To", config.TargetMail)
			m.Attach(filepath.Join(folderPath, item.Name()))

			if err := d.DialAndSend(m); err != nil {
				log.Fatal(err)
			}
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "upload done: "+item.Name())
			msg.ReplyToMessageID = update.Message.MessageID
			bot.Send(msg)
		}
	}
}
